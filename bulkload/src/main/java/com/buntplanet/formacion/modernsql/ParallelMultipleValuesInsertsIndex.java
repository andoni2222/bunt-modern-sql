package com.buntplanet.formacion.modernsql;

import javax.sql.DataSource;
import java.sql.Connection;

class ParallelMultipleValuesInsertsIndex {
  public static void main(String[] args) throws Throwable {
    DataSource ds = Setup.getDataSource();
    Connection conn = ds.getConnection();
    Setup.prepareDBNoIndexes(conn);
    Setup.createIndexes(conn);
    conn.close();
    InsertModes.parallelMultipleValues(1000, ds);
  }
}