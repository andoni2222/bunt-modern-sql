package com.buntplanet.formacion.modernsql;

import javax.sql.DataSource;
import java.sql.Connection;

class ParallelMultipleValuesInserts {
  public static void main(String[] args) throws Throwable {
    DataSource ds = Setup.getDataSource();
    Connection conn = ds.getConnection();
    Setup.prepareDBNoIndexes(conn);
    conn.close();
    InsertModes.parallelMultipleValues(10_000, ds);
  }
}