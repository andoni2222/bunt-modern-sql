package com.buntplanet.formacion.modernsql;

import java.sql.Connection;

public class IndividualInserts {
  public static void main(String[] args) throws Throwable {
    Connection conn = Setup.getSingleConnection();

    Setup.prepareDBNoIndexes(conn);

    InsertModes.individualInserts(conn);

    conn.close();
  }
}
