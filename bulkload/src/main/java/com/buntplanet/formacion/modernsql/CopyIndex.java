package com.buntplanet.formacion.modernsql;

import java.sql.Connection;

public class CopyIndex {
  public static void main(String[] args) throws Throwable {
    Connection conn = Setup.getSingleConnection();
    Setup.prepareDBNoIndexes(conn);
    Setup.createIndexes(conn);
    InsertModes.copy(conn);
    conn.close();
  }
}
