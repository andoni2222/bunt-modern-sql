package com.buntplanet.formacion.modernsql;

import java.sql.Connection;

public class Copy {
  public static void main(String[] args) throws Throwable {
    Connection conn = Setup.getSingleConnection();
    Setup.prepareDBNoIndexes(conn);
    InsertModes.copy(conn);
    conn.close();
  }
}
